import axios from 'axios';

import { getItemsApi } from 'helpers/apiHelper';
import { BASE_URL } from 'constants/api';

export const getCharactersApi = async (params) => {
  const response = await axios.get(`${BASE_URL}/character`, { params });

  return getItemsApi(response);
};

export const getLocationsApi = async () => {
  const response = await axios.get(`${BASE_URL}/location`);

  return getItemsApi(response);
};

export const getEpisodesApi = async () => {
  const response = await axios.get(`${BASE_URL}/episode`);

  return getItemsApi(response);
};
