import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import App from 'app';
import * as serviceWorker from './serviceWorker';
import createStore from './store/create-store';

import 'styles/index.scss';

const history = createBrowserHistory();
const store = createStore({}, history);

const Root = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>
);

render(<Root />, document.getElementById('root'));

serviceWorker.unregister();
