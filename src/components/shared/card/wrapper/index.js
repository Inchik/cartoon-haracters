import React from 'react';
import PropTypes from 'prop-types';

import s from './styles.module.scss';

const Wrapper = ({ children }) => <div className={s.wrapperCard}>{children}</div>;

Wrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Wrapper;
