import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { getLowerCase } from 'helpers/global';
import Wrapper from 'components/shared/card/wrapper';
import { STATUSES } from 'constants/global';

import s from './styles.module.scss';

const Card = (props) => {
  const { name, imageUrl, status, species, location } = props;

  const iconStyle = cx(s.statusIcon, {
    [s.statusAlive]: getLowerCase(status) === STATUSES.ALIVE,
    [s.statusDead]: getLowerCase(status) === STATUSES.DEAD,
    [s.statusUnknown]: getLowerCase(status) === STATUSES.UNKNOWN,
  });
  return (
    <Wrapper>
      <div className={s.wrapperAvatar}>
        <img alt={name} src={imageUrl} className={s.avatar} />
      </div>
      <div className={s.wrapperContent}>
        <div className={s.section}>
          <Link to="#">
            <h2>{name}</h2>
          </Link>
          <span className={s.status}>
            <span className={iconStyle} />
            {status} - {species}
          </span>
        </div>
        <div className={s.section}>
          <span className={s.textGray}>Last known location:</span>
          <Link className={s.simpleLink} to="#">
            {location}
          </Link>
        </div>
        <div className={s.section}>
          <span className={s.textGray}>First seen in:</span>
          <Link className={s.simpleLink} to="#">
            Tomorrowland 2020
          </Link>
        </div>
      </div>
    </Wrapper>
  );
};

Card.propTypes = {
  name: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  species: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
};

export default Card;
