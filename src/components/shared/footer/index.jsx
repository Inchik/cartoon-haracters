import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { appState } from 'store/modules/app/selectors';
import { RESOURCES } from 'constants/global';

import s from './styles.module.scss';

const Footer = () => {
  const { characterCount, locationCount, episodeCount } = useSelector(appState);
  return (
    <footer className={s.footer}>
      <ul className={s.resourceList}>
        <li className={s.resourceItem}>
          <span>
            {RESOURCES.CHARACTERS}: {characterCount}
          </span>
        </li>
        <li className={s.resourceItem}>
          <span>
            {RESOURCES.LOCATIONS}: {locationCount}
          </span>
        </li>
        <li className={s.resourceItem}>
          <span>
            {RESOURCES.EPISODES}: {episodeCount}
          </span>
        </li>
      </ul>
      <Link to="#" className={s.status}>
        <span className={s.statusCaption}>server status</span>
        <span className={s.statusIcon} />
      </Link>
    </footer>
  );
};

export default Footer;
