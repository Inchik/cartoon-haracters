import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

export default function useDidMount(action) {
  const dispatch = useDispatch();

  return useEffect(() => {
    dispatch(action());
  }, [dispatch, action]);
}
