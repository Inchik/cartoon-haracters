import { createActions } from 'redux-actions';

export const {
  loadResources,
  loadResourcesSuccess,
  loadResourcesFailure,
  setIsLoading,

  setCharacters,
  setCharactersError,
  setIsLoadingCharacters,

  setLocations,
  setLocationsError,

  setEpisodes,
  setEpisodesError,

  changeFilter,
  setChangeFilter,
  setChangeFilterError,
} = createActions(
  'LOAD_RESOURCES',
  'LOAD_RESOURCES_SUCCESS',
  'LOAD_RESOURCES_FAILURE',
  'SET_IS_LOADING',
  'GET_CHARACTERS',
  'SET_CHARACTERS',
  'SET_CHARACTERS_ERROR',
  'GET_LOCATIONS',
  'SET_LOCATIONS',
  'SET_LOCATIONS_ERROR',
  'CHANGE_FILTER',
  'SET_CHANGE_FILTER',
  'SET_CHANGE_FILTER_ERROR',
  'SET_IS_LOADING_CHARACTERS',
  'SET_EPISODES',
  'SET_EPISODES_ERROR',
  {
    prefix: 'APP',
  },
);
