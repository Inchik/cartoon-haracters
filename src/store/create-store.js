import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';

import rootSaga from 'store/sagas';
import createRootReducer from 'store/modules/create-root-reducer';

const sagaMiddleware = createSagaMiddleware();

const getEnhancers = (history) => {
  const historyMiddleware = routerMiddleware(history);
  const middleware = [sagaMiddleware, historyMiddleware];
  const enhancers = applyMiddleware(...middleware);
  let composeEnhancers = (e) => e;

  if (process.env.NODE_ENV === 'development') {
    composeEnhancers =
      typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
        : compose;
  }

  return composeEnhancers(enhancers);
};

export default (preloadedState, history) => {
  const store = createStore(createRootReducer(history), preloadedState, getEnhancers(history));

  sagaMiddleware.run(rootSaga);

  return store;
};
