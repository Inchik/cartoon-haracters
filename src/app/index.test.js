import React from 'react';
import { render } from '@testing-library/react';
import App from 'app/index';

import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import createStore from 'store/create-store';

const ReduxProvider = ({ children, reduxStore }) => (
  <Provider store={reduxStore}>{children}</Provider>
);

test('renders learn react link', () => {
  const history = createBrowserHistory();
  const store = createStore({}, history);
  render(
    <ReduxProvider reduxStore={store}>
      <App />
    </ReduxProvider>,
  );
});
