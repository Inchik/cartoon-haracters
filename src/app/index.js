import React from 'react';

import RootRouter from 'router';

import s from './styles.module.scss';

const App = () => (
  <div className={s.root}>
    <RootRouter />
  </div>
);

export default App;
