import { useDispatch, useSelector } from 'react-redux';
import { useCallback, useEffect } from 'react';
import debounce from 'lodash/debounce';

import { appState } from 'store/modules/app/selectors';
import { changeFilter } from 'store/modules/app/actions';

export default () => {
  const { characters, isLoadingCharacters } = useSelector(appState);

  const dispatch = useDispatch();

  const onHandleChangeFilter = useCallback(
    debounce(() => {
      dispatch(changeFilter());
    }, 500),
    [dispatch],
  );

  const listener = useCallback(() => {
    const scrollBarHeight = window.innerHeight * (window.innerHeight / document.body.offsetHeight);

    const { body } = document;
    const html = document.documentElement;

    const height = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );

    if (height - (window.pageYOffset + scrollBarHeight) < 300) {
      onHandleChangeFilter();
    }
  }, [onHandleChangeFilter]);

  useEffect(() => {
    window.addEventListener('scroll', listener);

    return () => {
      window.removeEventListener('scroll', listener);
    };
  }, [listener]);

  return {
    characters,
    isLoadingCharacters,
  };
};
