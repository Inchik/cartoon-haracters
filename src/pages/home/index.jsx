import React from 'react';
import cx from 'classnames';

import Card from 'components/shared/card';
import useStore from './useStore';

import s from './styles.module.scss';

const Home = () => {
  const { characters, isLoadingCharacters: isLoading } = useStore();

  return (
    <main>
      <section className={s.title}>
        <h1>The Rick and Morty API</h1>
      </section>
      <section className={cx(s.wrapperCardList, { [s.isLoading]: isLoading })}>
        <div className={s.cardList}>
          {characters.map((item) => {
            const {
              id,
              name,
              image,
              status,
              species,
              location: { name: locationName },
            } = item;
            return (
              <Card
                key={id}
                name={name}
                imageUrl={image}
                status={status}
                species={species}
                location={locationName}
              />
            );
          })}
        </div>
        {isLoading && (
          <div className={s.loading}>
            <span>Loading...</span>
          </div>
        )}
      </section>
    </main>
  );
};

export default Home;
